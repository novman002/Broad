public class MainActivity extends ActionBarActivity implements OnClickListener {

        TextView lblLocation, lblTemperature, lblPressure, lblDescription,
                lblHumidity, lblWind, lblWindDegree;
        EditText txtCity;
        Button btnSubmit;

        String cityStr, temperatureStr, locationStr;
        Context context;
        ProgressDialog progressDialog;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            context = this;

            lblLocation = (TextView) findViewById(R.id.lbl_location);
            lblTemperature = (TextView) findViewById(R.id.lbl_temperature);
            lblPressure = (TextView) findViewById(R.id.lbl_pressure);
            lblDescription = (TextView) findViewById(R.id.lbl_description);
            lblHumidity = (TextView) findViewById(R.id.lbl_humidity);
            lblWind = (TextView) findViewById(R.id.lbl_wind);
            lblWindDegree = (TextView) findViewById(R.id.lbl_wind_degree);

            txtCity = (EditText) findViewById(R.id.txt_city);
            btnSubmit = (Button) findViewById(R.id.btn_submit);
            btnSubmit.setOnClickListener(this);
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            int id = item.getItemId();
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onClick(View v) {
            cityStr = txtCity.getText().toString();
            if (cityStr.length() != 0) {
                retriveWeather(context, cityStr);

            } else {
                WeatherAppUtils.showMessage(getApplicationContext(),
                        "Please enter City Name");
            }
        }

        private void retriveWeather(Context c, String city) {
            showProgressDialog(c);
            WeatherAppApi.retriveWeatherByCity(c, city, new OnGetWeatherCallBack() {

                @Override
                public void onSuccess(Weather weather) {
                    if (weather != null) {
                        dismissProgressDialog();
                        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                        locationStr = weather.getCityStr() +  ", " + weather.getCountryStr();
                        lblLocation.setText(locationStr.toString());
                        temperatureStr = decimalFormat.format(weather.getMaxTemp())
                                + "/" + decimalFormat.format(weather.getMinTemp())
                                + " \u2103";
                        lblTemperature.setText(temperatureStr);
                        lblHumidity.setText(weather.getHumidity() + " %");
                        lblWind.setText(weather.getWindSpeed() + " mph");
                        lblWindDegree.setText(decimalFormat.format(weather.getWindDegree()) + " \u00b0");
                        lblDescription.setText(weather.getDescription());
                    }
                }

                @Override
                public void onStatusFailure(String message) {
                    WeatherAppUtils.showMessage(getApplicationContext(), message);
                }

                @Override
                public void onFailure(String message) {
                    WeatherAppUtils.showMessage(getApplicationContext(), message);
                }
            });
        }

        private void showProgressDialog(Context c) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(c);
            }
            progressDialog.setMessage("Please wait ...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        private void dismissProgressDialog() {
            progressDialog.dismiss();
        }
    }